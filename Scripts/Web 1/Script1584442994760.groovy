import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://ugm.ac.id/')

WebUI.click(findTestObject('Object Repository/Page_Universitas Gadjah Mada/span_Toggle navigation_icon-bar'))

WebUI.click(findTestObject('Object Repository/Page_Universitas Gadjah Mada/a_MAHASISWA'))

WebUI.click(findTestObject('Object Repository/Page_Direktorat Kemahasiswaan  Universitas _659ba8/span_Toggle navigation_icon-bar'))

WebUI.waitForElementClickable(findTestObject('Page_Direktorat Kemahasiswaan  Universitas _659ba8/a_PRESTASI 2019'), 3)

WebUI.click(findTestObject('Object Repository/Page_Direktorat Kemahasiswaan  Universitas _659ba8/a_PRESTASI 2019'))

WebUI.verifyElementClickable(findTestObject('Page_Universitas Gadjah Mada/a_MAHASISWA'))

WebUI.closeBrowser()

