import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://bitbucket.org/')

WebUI.click(findTestObject('Page_Bitbucket  The Git solution for profes_aec737/a_Log in'))

WebUI.setText(findTestObject('Page_Log in to continue - Log in with Atlas_6762ee/input_Bitbucket_username'), 'grace.hutabarat@si.ukdw.ac.id')

WebUI.click(findTestObject('Page_Log in to continue - Log in with Atlas_6762ee/span_Continue'))

WebUI.setText(findTestObject('Page_Log in to continue - Log in with Atlas_6762ee/input_Bitbucket_username'), 'grace.hutabarat@si.ukdw.ac.id')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Log in to continue - Log in with Atlas_6762ee/input_gracehutabaratsiukdwacid_password'), 
    'f9vNws2wOTSLOcLx7NQ1Cw==')

WebUI.click(findTestObject('Page_Log in to continue - Log in with Atlas_6762ee/span_Log in'))

WebUI.click(findTestObject('Object Repository/Page_Overview  Bitbucket/span'))

WebUI.waitForElementClickable(findTestObject('Page_Bitbucket/div_Snippets'), 5)

WebUI.click(findTestObject('Object Repository/Page_Overview  Bitbucket/a_View profile'))

WebUI.verifyElementClickable(findTestObject('Page_Overview  Bitbucket/a_View profile'), FailureHandling.STOP_ON_FAILURE)

