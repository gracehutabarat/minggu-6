import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('D:\\KULIAH\\PROJECTProgmob-master\\app\\build\\outputs\\apk\\debug\\app-debug.apk', true)

Mobile.tap(findTestObject('android.widget.EditText0 - Email (3)'), 0)

Mobile.setText(findTestObject('android.widget.EditText0 - Email (3)'), 'grace@staff.ukdw.ac.id', 0)

Mobile.setText(findTestObject('android.widget.EditText0 - Password (5)'), '12345', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('android.widget.Button0 - SIGN IN (3)'), 0)

Mobile.tap(findTestObject('android.widget.TextView0 - SI KRS - ADMIN-HAI ARY'), 0)

Mobile.tap(findTestObject('android.widget.ImageButton0 (2)'), 0)

Mobile.tap(findTestObject('android.widget.ImageView0 (2)'), 0)

Mobile.tap(findTestObject('android.widget.TextView0 - Create (2)'), 0)

Mobile.setText(findTestObject('android.widget.EditText0 - Alpro-1 (3)'), '001PPL', 0)

Mobile.tap(findTestObject('android.widget.EditText0 - Alpro (3)'), 0)

Mobile.setText(findTestObject('android.widget.EditText0 - Alpro (4)'), 'PPL', 0)

Mobile.setText(findTestObject('android.widget.EditText0 - 3 (2)'), '2', 0)

Mobile.tap(findTestObject('android.widget.TextView0 - Senin (1)'), 0)

Mobile.tap(findTestObject('android.widget.TextView0 - Selasa (2)'), 0)

Mobile.tap(findTestObject('android.widget.TextView0 - Sesi1 (1)'), 0)

Mobile.tap(findTestObject('android.widget.TextView0 - Sesi4 (2)'), 0)

Mobile.tap(findTestObject('android.view.ViewGroup0'), 0)

Mobile.tap(findTestObject('android.widget.Button0 - YES (1)'), 0)

navBarDashboard = Mobile.getText(findTestObject('Minggu11_AndroidPath/android.widget.TextView0 - SI KRS - ADMIN-HAI ARY (1)'), 
    0)

if (navBarDashboard == 'SI KRS - ADMIN-HAI ARY') {
    WebUI.comment('Berhasil')
} else {
    WebUI.comment('Gagal')
}

Mobile.tap(findTestObject('Minggu11_AndroidPath/android.widget.TextView0 (2)'), 0)

Mobile.tap(findTestObject('Minggu11_AndroidPath/android.widget.Button0 - Yes (5)'), 0)

Mobile.closeApplication()

