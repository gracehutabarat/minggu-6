<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Uji Coba Login dengan Data Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d48a2134-d245-4371-a3d5-893a865b2b72</testSuiteGuid>
   <testCaseLink>
      <guid>e82ddb1a-3676-42ab-a8d4-d121b306fb28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Meet 10/Test Case Test Login - Variabel</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>655e0d33-5ded-4d4a-abe9-d93496d122d8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Login</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>655e0d33-5ded-4d4a-abe9-d93496d122d8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>8f8ccdc3-059f-4467-90d5-799a455c6d26</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>655e0d33-5ded-4d4a-abe9-d93496d122d8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>56d18d5d-583d-471a-9aa9-cc180fb5f304</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
